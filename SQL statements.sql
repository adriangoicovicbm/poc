

-- Create a new database called 'dbMicroService'
-- Connect to the 'master' database to run this snippet
USE dbMicroService
GO
-- Create the new database if it does not exist already

-- Create a new table called 'TableName' in schema 'SchemaName'
-- Drop the table if it already exists
IF OBJECT_ID('CONTACTO', 'U') IS NOT NULL
DROP TABLE CONTACTO
GO
-- Create the table in the specified schema
CREATE TABLE CONTACTO
(
    Id INT NOT NULL PRIMARY KEY, -- primary key column
    Nombre [NVARCHAR](100) NOT NULL,
    Email [NVARCHAR](100) NOT NULL,
    FechaNacimiento [NVARCHAR](100) NOT NULL
    -- specify more columns here
);
GO

-- Insert rows into table 'CONTACTO'
INSERT INTO CONTACTO
( -- columns to insert data into
 [Id], [Nombre], [Email],[FechaNacimiento]
)
VALUES
( -- first row: values for the columns in the list above
 1, 1, 1,1
),
( -- second row: values for the columns in the list above
 2, 2, 2,2
),
( -- second row: values for the columns in the list above
 3, 3, 3,3
)
-- add more rows here
GO

SELECT * from CONTACTO
GO
