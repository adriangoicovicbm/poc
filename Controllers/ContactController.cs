using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using contactos.Models;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace contactos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController: ControllerBase
    {
        private readonly ContactoContext _context;

        public ContactController(ContactoContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public IEnumerable<Contacto> Get()
        {
                                    
            HttpWebRequest request;
            request = WebRequest.Create("https://5f85ce9cc8a16a0016e6a568.mockapi.io/contacto") as HttpWebRequest;
            request.Timeout = 10 * 100000;
            request.Method = "GET";
            request.ContentType = "application/json; charset=utf-8";

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string body = reader.ReadToEnd();
            
            JArray stuff = JArray.Parse(body);
            
            List<Contacto> contactos = new List<Contacto>();
            
            foreach(var item in stuff)
            {
                
                Contacto contacto = new Contacto();
                contacto.Id = Int32.Parse(item["id"].ToString());
                contacto.Nombre = item["nombre"].ToString();
                contacto.Email = item["email"].ToString();
                contacto.FechaNacimiento = item["fechaNacimiento"].ToString();
                
                contactos.Add(contacto);
               
            }
            return contactos.ToArray();

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Contacto>> GetById(int id)
        {
            var item = await _context.Contacto.FindAsync(id);
            if(item==null)
                return NotFound();

            return item;
        }
        [HttpPost]
        public async Task<ActionResult<Contacto>> Create(Contacto item)
        {
            if(item== null)
            {
                return BadRequest();
            }

            _context.Contacto.Add(item);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetById),new {id = item.Id},item);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Contacto>> Update(int id,[FromBody]Contacto item)
        {
            if(item == null || id == 0)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Contacto>> Delete(int id)
        {
            var item = await _context.Contacto.FindAsync(id);
            if(item == null || id == 0)
            {
                return NotFound();
            }
            _context.Contacto.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /*
        [HttpGet]
        public IEnumerable<Contacto> GetAll()
        {
            return _context.Contacto.ToList();

        }
        */
      

    }


}