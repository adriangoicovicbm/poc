using Microsoft.EntityFrameworkCore;

namespace contactos.Models
{
    public class ContactoContext : DbContext
    {
        public ContactoContext(DbContextOptions<ContactoContext> options)
        :base(options)
        {

        }
        public DbSet<Contacto> Contacto {get;set;}
    }
}