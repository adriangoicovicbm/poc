using System;
using System.ComponentModel.DataAnnotations;
using Validaciones;

namespace contactos.Models
{
    public class Contacto
    {
        public int Id {get;set;}
        public string Nombre{get;set;}
        [EmailAddress]
        [ValidarEmail(email: "caca")]
        public string Email{get;set;}
        public string FechaNacimiento {get;set;}

    }
}